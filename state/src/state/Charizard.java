package state;

public class Charizard extends PelihahmonTila {
    private static Charizard instance = null;
    
    private Charizard() {
    }
    
    public static Charizard getInstance() {
        if (instance == null) {
            instance = new Charizard();
        }
        return instance;
    }
    
    void hyokkaa(Pelihahmo p){
        System.out.println("Charizard teki 6 vahinkopistettä vastustajaan");
    }
    void huuda(Pelihahmo p){
        System.out.println("MURR! Olen suurin ja vahvin Charizard");
    }
    void kehity(Pelihahmo p){
        
    }
    
    void regen(Pelihahmo p, int hp) {
        p.addHealth(hp);
    }
    void changeState(Pelihahmo p, PelihahmonTila t) {
        p.changeState(t);
    }
    
}
