package state;

public class Charmander extends PelihahmonTila {
    
    private static Charmander instance = null;
    
    private Charmander() {
    }
    
    public static Charmander getInstance() {
        if (instance == null) {
            instance = new Charmander();
        }
        return instance;
    }
    
    void hyokkaa(Pelihahmo p){
        System.out.println("Charmander teki 2 vahinkopistettä vastustajaan");
    }
    void huuda(Pelihahmo p){
        System.out.println("murr, olen Charmander");
    }
    void kehity(Pelihahmo p){
        System.out.println("kehitys!");
        regen(p, 40);
        changeState(p, Charmeleon.getInstance());
    }
    void regen(Pelihahmo p, int hp) {
        p.addHealth(hp);
    }
    
    void changeState(Pelihahmo p, PelihahmonTila t) {
        p.changeState(t);
    }
    
}
