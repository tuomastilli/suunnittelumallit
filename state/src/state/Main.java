package state;

public class Main {

    public static void main(String[] args) {
        Pelihahmo pokemon = new Pelihahmo();
        pokemon.hyokkaa();
        System.out.println("hp: " + pokemon.getHealth());
        pokemon.kehity();
        pokemon.huuda();
        System.out.println("hp: " + pokemon.getHealth());
        pokemon.kehity();
        pokemon.huuda();
        System.out.println("hp: " + pokemon.getHealth());
        
    }

}
