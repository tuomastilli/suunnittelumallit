package state;

public abstract class PelihahmonTila {
    void hyokkaa(Pelihahmo p){}
    void huuda(Pelihahmo p){}
    void kehity(Pelihahmo p){}
    void regen(Pelihahmo p, int hp) {
    }
    void changeState(Pelihahmo p, PelihahmonTila t) {
        p.changeState(t);
    }
}