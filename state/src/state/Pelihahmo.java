package state;

public class Pelihahmo {
    private PelihahmonTila state;
    private int hp;

    public Pelihahmo() {
        this.state = Charmander.getInstance();
        this.hp = 100;
    }
    
    public void hyokkaa() {
        state.hyokkaa(this);
    }
    public void huuda() {
        state.huuda(this);
    }
    public void kehity() {
        state.kehity(this);
    }
    
    public void addHealth(int hp) {
        this.hp += hp;
    }
    public int getHealth() {
        return this.hp;
    }
    
    protected void changeState(PelihahmonTila t) {
        state = t;
    }
    
    
}
