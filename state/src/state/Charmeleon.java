package state;

public class Charmeleon extends PelihahmonTila {
    private static Charmeleon instance = null;
    
    private Charmeleon() {
    }
    
    public static Charmeleon getInstance() {
        if (instance == null) {
            instance = new Charmeleon();
        }
        return instance;
    }
    
    void hyokkaa(Pelihahmo p){
        System.out.println("Charmeleon teki 4 vahinkopistettä vastustajaan");
    }
    void huuda(Pelihahmo p){
        System.out.println("MURR! Olen iso Charmeleon");
    }
    void kehity(Pelihahmo p){
        System.out.println("kehitys!");
        regen(p, 40);
        changeState(p, Charizard.getInstance());
    }
    void regen(Pelihahmo p, int hp) {
        p.addHealth(hp);
    }
    void changeState(Pelihahmo p, PelihahmonTila t) {
        p.changeState(t);
    }
}
