package composite;

public class PremiumVerkkokortti implements Laiteosa {
    private int hinta;

    public PremiumVerkkokortti() {
        this.hinta = 80;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
