
package composite;

public interface TehdasIF {
    public abstract Laiteosa luoKotelo();
    public abstract Laiteosa luoEmolevy();
    public abstract Laiteosa luoProsessori();
    public abstract Laiteosa luoMuistipiiri();
    public abstract Laiteosa luoNaytonohjain();
    public abstract Laiteosa luoVerkkokortti();
    public abstract Laiteosa luoVirtalahde();
}
