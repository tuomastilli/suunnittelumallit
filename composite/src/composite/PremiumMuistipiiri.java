package composite;

public class PremiumMuistipiiri implements Laiteosa {
    private int hinta;
    
    public PremiumMuistipiiri() {
        this.hinta = 130;
    }

    public int getHinta() {
        return this.hinta;
    }

    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
