package composite;

public class BasicVirtalahde implements Laiteosa {
    private int hinta;

    public BasicVirtalahde() {
        this.hinta = 70;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
