package composite;

public class PremiumNaytonohjain implements Laiteosa {
    private int hinta;

    public PremiumNaytonohjain() {
        this.hinta = 500;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
