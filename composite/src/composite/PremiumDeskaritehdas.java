package composite;

public class PremiumDeskaritehdas implements TehdasIF{

    public Laiteosa luoKotelo() {
        return new PremiumKotelo();
    }

    public Laiteosa luoEmolevy() {
        return new PremiumEmolevy();
    }

    public Laiteosa luoProsessori() {
        return new PremiumProsessori();
    }

    public Laiteosa luoMuistipiiri() {
        return new PremiumMuistipiiri();
    }

    public Laiteosa luoNaytonohjain() {
        return new PremiumNaytonohjain();
    }

    public Laiteosa luoVerkkokortti() {
        return new PremiumVerkkokortti();
    }

    public Laiteosa luoVirtalahde() {
        return new PremiumVirtalahde();
    }
    
}
