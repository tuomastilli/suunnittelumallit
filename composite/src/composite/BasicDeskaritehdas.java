package composite;

public class BasicDeskaritehdas implements TehdasIF {

    public Laiteosa luoKotelo() {
        return new BasicKotelo();
    }

    public Laiteosa luoEmolevy() {
        return new BasicEmolevy();
    }

    public Laiteosa luoProsessori() {
        return new BasicProsessori();
    }

    public Laiteosa luoMuistipiiri() {
        return new BasicMuistipiiri();
    }

    public Laiteosa luoNaytonohjain() {
        return new BasicNaytonohjain();
    }

    public Laiteosa luoVerkkokortti() {
        return new BasicVerkkokortti();
    }

    public Laiteosa luoVirtalahde() {
        return new BasicVirtalahde();
    }
    
}
