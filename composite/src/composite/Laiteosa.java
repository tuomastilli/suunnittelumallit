package composite;

public interface Laiteosa {
    public int getHinta();
    public void addLaiteosa(Laiteosa laiteosa);
}
