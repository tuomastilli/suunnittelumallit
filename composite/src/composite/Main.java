package composite;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import composite.*;

public class Main {

    public static void main(String[] args) {
        //File file = new File(".");
        //for(String fileNames : file.list()) System.out.println(fileNames);
        
        Laiteosa mylly = kasaaMylly("composite.BasicDeskaritehdas");
        System.out.println("Peruskoneen hinta: " + mylly.getHinta());
        mylly = kasaaMylly("composite.PremiumDeskaritehdas");
        System.out.println("Premiumkoneen hinta: " + mylly.getHinta());
    }
    
    public static Laiteosa kasaaMylly(String tehtaanNimi) {
        Class c = null;
        TehdasIF tehdas = null;
        Laiteosa kotelo = null;
        
        try {
            c = Class.forName(tehtaanNimi);
            tehdas = (TehdasIF) c.getDeclaredConstructor().newInstance();
            
            kotelo = tehdas.luoKotelo();
            Laiteosa muisti = tehdas.luoMuistipiiri();
            Laiteosa prossu = tehdas.luoProsessori();
            Laiteosa verkkokortti = tehdas.luoVerkkokortti();
            Laiteosa nayttis = tehdas.luoNaytonohjain();
            Laiteosa virtis = tehdas.luoVirtalahde();

            Laiteosa emo = tehdas.luoEmolevy();
            emo.addLaiteosa(prossu);
            emo.addLaiteosa(muisti);
            emo.addLaiteosa(nayttis);
            emo.addLaiteosa(verkkokortti);

            kotelo.addLaiteosa(virtis);
            kotelo.addLaiteosa(emo);
            
        } catch (Exception e) { e.printStackTrace(); }
        
        return kotelo;
    }
    
}