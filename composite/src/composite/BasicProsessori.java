package composite;

public class BasicProsessori implements Laiteosa {
    private int hinta;

    public BasicProsessori() {
        this.hinta = 150;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
