package composite;

public class BasicVerkkokortti implements Laiteosa {
    private int hinta;

    public BasicVerkkokortti() {
        this.hinta = 50;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
