package composite;

import java.util.ArrayList;
import java.util.List;

public class BasicEmolevy implements Laiteosa {
    private int hinta;
    private List<Laiteosa> osat;

    public BasicEmolevy() {
        this.hinta = 120;
        this.osat = new ArrayList<Laiteosa>();
    }
    
    public int getHinta() {
        int summa = 0;
        for (Laiteosa l : this.osat) {
            summa += l.getHinta();
        }
        return summa + this.hinta;
    }
    
    public void addLaiteosa(Laiteosa lisattavaLaiteosa) {
        this.osat.add(lisattavaLaiteosa);
    }
    
    
    
}
