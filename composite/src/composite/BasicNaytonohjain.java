package composite;

public class BasicNaytonohjain implements Laiteosa {
    private int hinta;

    public BasicNaytonohjain() {
        this.hinta = 200;
    }
    
    public int getHinta() {
        return this.hinta;
    }
    
    public void addLaiteosa(Laiteosa laiteosa) {
        throw new RuntimeException
        ("Ei voi lisätä laiteosaa Leaf-osaan");
    }
    
    
    
}
