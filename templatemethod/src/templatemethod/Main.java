package templatemethod;


public class Main {

    public static void main(String[] args) {
        int pelaajienLkm = 2;
        int kierrokset = 2;
        
        Game game = new TruthOrDare(kierrokset);
        game.playOneGame(pelaajienLkm);
    }

}
