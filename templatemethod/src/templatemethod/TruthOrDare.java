package templatemethod;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

public class TruthOrDare extends Game {
    Properties truthProperties, dareProperties;
    int kierrokset, vuorot;
    Scanner scanner = new Scanner(System.in);

    public TruthOrDare(int kierrokset) {
        this.kierrokset = kierrokset;
        this.vuorot = 0;
    }
    
    @Override
    void initializeGame() {
        truthProperties = new Properties();
        dareProperties = new Properties();
        try {
            truthProperties.load(new FileInputStream("./src/templatemethod/truth.properties"));
            dareProperties.load(new FileInputStream("./src/templatemethod/dare.properties"));
        } catch (IOException e) { e.printStackTrace(); }
    }

    @Override
    void makePlay(int pelaaja) {
        int valinta = 0;
        Random random = new Random();
        
        System.out.println("Vuorossa pelaaja " + pelaaja + "!");
        System.out.println("");
        System.out.println("Totuus: paina 1    ||    Tehtävä: paina 2");
        
        while ((valinta != 1) && (valinta != 2)) {
            valinta = scanner.nextInt();
        }
        
        if (valinta == 1) {
            int totuus = random.nextInt(truthProperties.size());
            System.out.println(truthProperties.getProperty(String.valueOf(totuus)));
        } else {
            int tehtava = random.nextInt(dareProperties.size());
            System.out.println(dareProperties.getProperty(String.valueOf(tehtava)));
        }
        
        System.out.println("");
        System.out.println("Paina enter, kun olet valmis.");
        String wait = scanner.nextLine();
        wait = scanner.nextLine();
        vuorot++;
    }

    @Override
    boolean endOfGame() {
        if (vuorot / pelaajat < kierrokset) {
            return false;
        }
        return true;
    }

    
}