package templatemethod;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public abstract class Game {
    protected int pelaajat;
    
    abstract void initializeGame();    
    abstract void makePlay(int pelaaja);
    abstract boolean endOfGame();
    void lopetus() {
        System.out.println("Peli on päättynyt.");
    }
    
    public final void playOneGame(int pelaajat) {
        this.pelaajat = pelaajat;
        initializeGame();
        int j = 0;
        while (!endOfGame()) {
            makePlay(j);
            j = (j + 1) % pelaajat;
        }
        lopetus();
    }
    
            
            
            
            
}
