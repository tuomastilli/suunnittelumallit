package singleton;

public class Main {

    public static void main(String[] args) {
        SukkaTehdasIF single = SukkaTehdasSingleton.getInstance();
        
        SukkaIF sukka = single.luoSukka();
        System.out.println("Luodun sukan materiaali: " + sukka.getMateriaali());
    }

}
