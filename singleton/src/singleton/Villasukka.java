package singleton;

class Villasukka implements SukkaIF {
    private String materiaali;
    public Villasukka() {
        this.materiaali = "merinovilla";
    }

    @Override
    public String getMateriaali() {
        return this.materiaali;
    }
    
}
