package singleton;

class TavallinenSukka implements SukkaIF {
    private String materiaali;
    
    public TavallinenSukka() {
        this.materiaali = "puuvilla";
    }
    @Override
    public String getMateriaali() {
        return this.materiaali;
    }
}
