package singleton;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SukkaTehdasSingleton implements SukkaTehdasIF{
    private static volatile SukkaTehdasSingleton instance = null;
    
    private SukkaTehdasSingleton(){
    }
    
    public static synchronized SukkaTehdasSingleton getInstance() {
        if (instance == null) {
            String s = getTyyppi("tyyppi");
            
            if (s.equals("Villasukkatehdas")) {
                try {
                    instance = VillasukkaTehdas.class.newInstance();
                } catch (Exception e) {e.printStackTrace();};
            } else {
                instance = new SukkaTehdasSingleton();
            }
        }
        return instance;
    }
    
    private static String getTyyppi(String s) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("./src/singleton/tehdas.properties"));
        } catch (IOException e) { e.printStackTrace(); }
        
        return properties.getProperty(s);
    }

    @Override
    public SukkaIF luoSukka() {
        return new TavallinenSukka();
    }
    
    public static class VillasukkaTehdas extends SukkaTehdasSingleton implements SukkaTehdasIF {
        @Override
        public SukkaIF luoSukka() {
            return new Villasukka();
        }
    }
    
}
