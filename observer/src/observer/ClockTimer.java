package observer;

import static java.lang.Thread.sleep;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClockTimer extends Observable implements Runnable {
    private int hour;
    private int min;
    private int sec;

    public ClockTimer(int hour, int min, int sec) {
        this.hour = hour;
        this.min = min;
        this.sec = sec;
        
    }

    public int getHour() {
        return this.hour;
    }
    public int getMin() {
        return this.min;
    }
    public int getSec() {
        return this.sec;
    }
    
    public void tick() {
        if (sec < 59) {
            sec++;
        } else if (sec == 59 && min < 59) {
            sec = 0;
            min++;
        } else if (sec == 59 && min == 59 && hour < 23) {
            min = 0;
            hour++;
        } else if (sec == 59 && min == 59 && hour == 23) {
            sec = 0;
            min = 0;
            hour = 0;
        }
        
        setChanged();
        notifyObservers();
    }

    public void run() {
        
        try {
            while (true) {
                sleep(1000);
                tick();
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ClockTimer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
