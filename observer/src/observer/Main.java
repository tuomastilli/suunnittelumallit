package observer;

import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {        
        
    ClockTimer eventSource = new ClockTimer(23,59,48);
    DigitalClock digitalclock = new DigitalClock(eventSource);

//    eventSource.addObserver(new Observer() {
//        ClockTimer ct;
//        public void update(Observable o, Object arg) {
//            if (o instanceof ClockTimer) {
//                ct = ClockTimer.class.cast(o);
//            System.out.println(ct.getHour() + ":" + ct.getMin() + ":" + ct.getSec());
//            }
//        }
//    });

    new Thread(eventSource).start();
    }
    
}