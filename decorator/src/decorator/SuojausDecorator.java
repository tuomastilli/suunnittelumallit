package decorator;

import java.util.Scanner;

public class SuojausDecorator extends DataDecorator {
    protected String salasana = null;
    
    public SuojausDecorator(Data dataToBeDecorated) {
        super(dataToBeDecorated);
        salasana = "vaikeapassu";
    }
    
    @Override
    public String getData() {
        if (salasana != null) {
            if (checkPassword()) {
                return super.getData();
            } else {
                return "ei pysty";
            }
        } else {
            return super.getData();
        }
    }
    
    private boolean checkPassword() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Anna salasana");
        String s = scanner.next();
        return s.equals(this.salasana);
    }
    
}
