package decorator;


public class Main {

    public static void main(String[] args) {
        Data data = new SimpleData("paljonpaljondataa");
        System.out.println(data.getData());
        data = new SuojausDecorator(data);
        System.out.println(data.getData());
        
        
    }
}
