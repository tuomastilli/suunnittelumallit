package decorator;

public class SimpleData implements Data {

    private String data;

    public SimpleData(String data) {
        this.data = data;
    }
    
    @Override
    public String getData() {
        return this.data;
    }
    
}
