package decorator;

public abstract class DataDecorator implements Data {
    protected Data dataToBeDecorated;

    public DataDecorator(Data dataToBeDecorated) {
        this.dataToBeDecorated = dataToBeDecorated;
    }
    
    public String getData() {
        return dataToBeDecorated.getData();
    }
    
}
