package decorator;

public interface Data {
    public abstract String getData();
}
