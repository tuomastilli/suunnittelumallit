
package abstractfactory;

public class BossFactory implements VaateFactory_IF {

    public BossFactory() {
    }

    @Override
    public Hattu_IF luoHattu() {
        return new BossHattu();
    }

    @Override
    public Paita_IF luoPaita() {
        return new BossKashmirNeule();
    }

    @Override
    public Housut_IF luoHousut() {
        return new BossFarkut();
    }

    @Override
    public Kengat_IF luoKengat() {
        return new BossKengat();
    }

    
    
}
