
package abstractfactory;

public class AdidasFarkut implements Housut_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public AdidasFarkut() {
        this.merkki = "Adidas";
        this.vaatetyyppi = "farkut";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
