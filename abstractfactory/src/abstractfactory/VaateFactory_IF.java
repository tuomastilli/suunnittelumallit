
package abstractfactory;

public interface VaateFactory_IF {
    public abstract Hattu_IF luoHattu();
    public abstract Paita_IF luoPaita();
    public abstract Housut_IF luoHousut();
    public abstract Kengat_IF luoKengat();
}
