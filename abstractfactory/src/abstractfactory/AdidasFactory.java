
package abstractfactory;

public class AdidasFactory implements VaateFactory_IF { 

    public AdidasFactory() {
    }

    @Override
    public Hattu_IF luoHattu() {
        return new AdidasLippis();
    }

    @Override
    public Paita_IF luoPaita() {
        return new AdidasTpaita();
    }

    @Override
    public Housut_IF luoHousut() {
        return new AdidasFarkut();
    }

    @Override
    public Kengat_IF luoKengat() {
        return new AdidasTossut();
    }
    
    

    
    
}
