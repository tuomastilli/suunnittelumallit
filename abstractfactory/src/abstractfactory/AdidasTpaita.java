
package abstractfactory;

public class AdidasTpaita implements Paita_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public AdidasTpaita() {
        this.merkki = "Adidas";
        this.vaatetyyppi = "t-paita";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
