
package abstractfactory;

public class AdidasTossut implements Kengat_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public AdidasTossut() {
        this.merkki = "Adidas";
        this.vaatetyyppi = "tossut";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
