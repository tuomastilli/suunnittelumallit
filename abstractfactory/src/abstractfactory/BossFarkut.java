
package abstractfactory;

public class BossFarkut implements Housut_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public BossFarkut() {
        this.merkki = "Boss";
        this.vaatetyyppi = "farkut";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
