package abstractfactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Jasper jasper = new Jasper();
        Properties properties = new Properties();
        
        try {
            properties.load(new FileInputStream("./src/abstractfactory/tehdas.properties"));
        } catch (IOException e) { e.printStackTrace(); }
        
        jasper.pueVaatteet(getTehdas(properties, "adidastehdas"));
        jasper.kerro();
        System.out.println("");
        
        jasper.pueVaatteet(getTehdas(properties, "bosstehdas"));
        jasper.kerro();
        
        
    }

    public static VaateFactory_IF getTehdas(Properties p, String s) {
        Class c = null;
        VaateFactory_IF vaatetehdas = null;
        try {
            c = Class.forName(p.getProperty(s));
            vaatetehdas = (VaateFactory_IF) c.getDeclaredConstructor().newInstance();
        } catch (Exception e) { e.printStackTrace(); }
        
        return vaatetehdas;
    }
}

