
package abstractfactory;

public class BossHattu implements Hattu_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public BossHattu() {
        this.merkki = "Boss";
        this.vaatetyyppi = "hattu";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
