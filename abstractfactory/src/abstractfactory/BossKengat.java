
package abstractfactory;

public class BossKengat implements Kengat_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public BossKengat() {
        this.merkki = "Boss";
        this.vaatetyyppi = "kengät";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
