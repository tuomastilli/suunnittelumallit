package abstractfactory;

import java.lang.reflect.*;

public class Jasper {
    
    private Hattu_IF hattu;
    private Paita_IF paita;
    private Housut_IF housut;
    private Kengat_IF kengat;

    public Jasper() {
    }
    
    public void pueVaatteet(VaateFactory_IF factory) {
        hattu = factory.luoHattu();
        paita = factory.luoPaita();
        housut = factory.luoHousut();
        kengat = factory.luoKengat();
    }
    
    public void kerro() {
        System.out.print("Minulla on päälläni: \n" + hattu.toString() + "\n" + paita.toString() + "\n" + housut.toString() + "\n" + kengat.toString() + "\n");
    }

    
}
