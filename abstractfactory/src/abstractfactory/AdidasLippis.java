
package abstractfactory;

public class AdidasLippis implements Hattu_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public AdidasLippis() {
        this.merkki = "Adidas";
        this.vaatetyyppi = "lippis";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
