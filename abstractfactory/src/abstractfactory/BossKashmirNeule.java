
package abstractfactory;

public class BossKashmirNeule implements Paita_IF {
    
    private String merkki;
    private String vaatetyyppi;

    public BossKashmirNeule() {
        this.merkki = "Boss";
        this.vaatetyyppi = "kashmirneule";
    }

    @Override
    public String toString() {
        return merkki + " " + vaatetyyppi;
    }
    
    
    
}
